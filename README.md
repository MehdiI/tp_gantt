# GANTT

Le but de ce projet est de permettre d'avoir une vision linéaire des développements en cours.

Celui si devra se connecter à FRAMAGIT via son [API](https://docs.gitlab.com/12.5/ee/api/README.html)

On récupére les projets d'un groupe donné pour en afficher pour chaque projets une projection des millestones et de leurs issues.

Mais aussi des issues sans millestone.

On pourra récupérer les date de fin `date_due` et les `estimate_date` 

L'application devra gérer les relations parent / enfant sur les issues mais aussi...

Potentiellement on gérera les priorités de dev entre millestone et inter projets.


